#ifndef COORD_H
#define COORD_H


class Coord
{
public:
    Coord();
    Coord(const int & _X,const int & _Y);
    Coord( const Coord & _coord);
    ~Coord();
    int x;
    int y;
};

#endif // COORD_H
