#ifndef PREDATOR_H
#define PREDATOR_H

#include "Object/prey.h"
#include "setting.h"

class Predator : public Prey
{
public:
    Predator(const Coord & _coord, SettingObjectPredator _setting);
    ~Predator();

    virtual Reaction action(Environment environment) override;
    void set_hangry();

private:
    SettingObjectPredator setting;

};

#endif // PREDATOR_H
