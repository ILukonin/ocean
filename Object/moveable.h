#ifndef MOVEABLE_H
#define MOVEABLE_H

#include "object.h"

class Moveable : public Object
{
public:
    Moveable(const Coord & _coord);
    ~Moveable();

     //Reaction action(Environment environment) override;


protected:
    Coord speed;

};

#endif // MOVEABLE_H
