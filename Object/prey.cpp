#include "Object/prey.h"

#include <vector>
#include <stdlib.h>

Prey::Prey(const Coord &_coord, SettingObjectPrey _setting):Moveable(_coord),setting(_setting)
{
    type = TYPE_OBJECT::PREY;
    lifetime = setting.lifetime;
}

Prey::~Prey()
{

}

Reaction Prey::action(Environment environment)
{
    Reaction reac;

    lifetime--;

    if( ( lifetime < 0) || (environment.set_death) )
    {
        reac.is_death = true;
        return reac;
    }

    reac.is_death = false;
    reac.new_object = TYPE_OBJECT::NONE;

    if( lifetime % setting.birth_count == 0)
        reac.new_object = type;


    std::vector<DIRECTION> direct; // создаем вектор для направлений

    if(environment.free_down)
        direct.push_back(DIRECTION::DOWN); // заполняем его свободными направлениями

    if(environment.free_up)
        direct.push_back(DIRECTION::UP);

    if(environment.free_left)
        direct.push_back(DIRECTION::LEFT);

    if(environment.free_right)
        direct.push_back(DIRECTION::RIGHT);

    if( direct.empty() )
    {
        reac.need_move = false; // если свободных направлений нет, то не двигаемся
        reac.direction = DIRECTION::RIGHT;
    } else
    {
        reac.need_move = true;
        reac.direction = direct[ rand() % direct.size() ];
    }



    return reac;
}
