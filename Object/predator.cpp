#include "Object/predator.h"

#include <vector>
#include <stdlib.h>

Predator::Predator(const Coord &_coord, SettingObjectPredator _setting):Prey(_coord, _setting),setting(_setting)
{
    type = TYPE_OBJECT::PREDATOR;
    lifetime = setting.lifetime;
    hangry = setting.hangry;
}

Predator::~Predator()
{

}

Reaction Predator::action(Environment environment)
{
    lifetime--;
    hangry--;

    Reaction reac;
    if( ( lifetime < 0) || (environment.set_death) || hangry < 0 )
    {
        reac.is_death = true;
        return reac;
    }

    reac.is_death = false;
    reac.new_object = TYPE_OBJECT::NONE;


    reac.need_move = false; // если свободных направлений нет, то не двигаемся
    reac.direction = DIRECTION::RIGHT;

    if(!reac.need_move)
    {
        std::vector<DIRECTION> direct; // создаем вектор для направлений

        if(TYPE_OBJECT::PREY == environment.neighbors_down)
            direct.push_back(DIRECTION::DOWN); // заполняем его свободными направлениями

        if(TYPE_OBJECT::PREY == environment.neighbors_up)
            direct.push_back(DIRECTION::UP);

        if(TYPE_OBJECT::PREY == environment.neighbors_left)
            direct.push_back(DIRECTION::LEFT);

        if(TYPE_OBJECT::PREY == environment.neighbors_right)
            direct.push_back(DIRECTION::RIGHT);

        if( direct.empty() )
        {
            reac.need_move = false; // если свободных направлений нет, то не двигаемся
            reac.direction = DIRECTION::RIGHT;
        } else
        {
            reac.need_move = true;
            reac.direction = direct[ rand() % direct.size() ];
        }
    }

    if(!reac.need_move)
    {
        std::vector<DIRECTION> direct; // создаем вектор для направлений

        if(environment.free_down)
            direct.push_back(DIRECTION::DOWN); // заполняем его свободными направлениями

        if(environment.free_up)
            direct.push_back(DIRECTION::UP);

        if(environment.free_left)
            direct.push_back(DIRECTION::LEFT);

        if(environment.free_right)
            direct.push_back(DIRECTION::RIGHT);

        if( direct.empty() )
        {
            reac.need_move = false; // если свободных направлений нет, то не двигаемся
            reac.direction = DIRECTION::RIGHT;
        } else
        {
            reac.need_move = true;
            reac.direction = direct[ rand() % direct.size() ];
        }
    }

    if(hangry > setting.birth_count)
    {
        reac.new_object = type;
        hangry = setting.hangry_after_birth;
    }



    return reac;

}

void Predator::set_hangry()
{
    hangry += setting.hangry;
}
