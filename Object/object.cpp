#include "Object/object.h"



Object::Object(const Coord &_coord):coord(_coord),type(TYPE_OBJECT::NONE)
{

}

Object::~Object()
{

}

void Object::set_position(Coord _coord)
{
    coord = _coord;
}

Coord Object::get_position() const
{
    return this->coord;
}

TYPE_OBJECT Object::get_type() const
{
    return type;
}

void Object::set_death()
{
    lifetime = 0;
}
