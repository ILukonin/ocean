#ifndef SETTING_H
#define SETTING_H


struct SettingObjectPrey
{
public:
    unsigned int lifetime;
    unsigned int birth_count;

};


struct SettingObjectPredator: public SettingObjectPrey
{
public:
    unsigned int hangry;
    unsigned int hangry_after_birth;
    unsigned int hangry_prey;

};

#endif // SETTING_H

