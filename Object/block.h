#ifndef BLOCK_H
#define BLOCK_H

#include "Object/object.h"

class Block : public Object
{
public:
    Block(const Coord & _coord);
    Reaction action(Environment environment) final;

    ~Block();


};

#endif // BLOCK_H
