#ifndef TREE_H
#define TREE_H

#include "Object/block.h"

class Tree : public Block
{
public:
    Tree(const Coord & _coord);
    ~Tree();
    //virtual Reaction action(Environment environment){ return * new Reaction;};
};

#endif // TREE_H
