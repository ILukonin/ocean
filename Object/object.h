#ifndef OBJECT_H
#define OBJECT_H

#include "type_object.h"
#include "coord.h"
#include "Game/reaction.h"
#include "Game/environment.h"
#include "Game/direction.h"



class Object
{
public:
     Object(const Coord & _coord);
     virtual ~Object();

     virtual Reaction action(Environment environment)=0; // делаем что-нибудь

     void set_position(Coord _coord); // установить координаты
     Coord get_position() const;     // получить координаты
     TYPE_OBJECT get_type() const;   // узнаём тип
     void set_death();



protected:
    static int count;
    Coord coord;
    TYPE_OBJECT type;
    int lifetime;


};

#endif // OBJECT_H
