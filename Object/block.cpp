#include "Object/block.h"

Block::Block(const Coord &_coord):Object(_coord)
{
    type = TYPE_OBJECT::STOUN;
}

Block::~Block()
{

}

Reaction Block::action(Environment environment)
{
    Reaction reac;
    reac.direction = DIRECTION::UP;
    reac.is_death = false;
    reac.need_move = false;
    reac.new_object = TYPE_OBJECT::NONE;

    return reac;
}
