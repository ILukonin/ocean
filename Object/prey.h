#ifndef PREY_H
#define PREY_H

#include "Object/moveable.h"
#include "setting.h"

class Prey : public Moveable
{
public:
    Prey(const Coord & _coord, SettingObjectPrey setting);
    ~Prey();
    virtual Reaction action(Environment environment) override;


protected:
    int hangry;

private:
    SettingObjectPrey setting;
};

#endif // PREY_H
