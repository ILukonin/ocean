#ifndef GAME_H
#define GAME_H

#include "coord.h"
#include "Object/type_object.h"
#include "Object/tree.h"
#include "Object/prey.h"
#include "Object/predator.h"
#include "status.h"
#include "Object/setting.h"
#include "settinggame.h"

#include <cstdlib>
#include <ctime>
#include <vector>



class Game
{
public:
    Game(SettingGame _setting);


    TYPE_OBJECT getObject(Coord cell);
    Status getStatus();

    void nextStep();

    ~Game();


protected:
    Coord size_field;
    TYPE_OBJECT ** current_field; // актуальное поле
    TYPE_OBJECT ** next_field;    // поле для следующего хода
    std::vector<Object *> array_object;       // это массив объектов наших
    Object *** array_field;       // массив для быстрого доступа к полю, чтоб не искать соседей

    void initial();               // инициализируем
    void swap_field();            // переключаем поля
    void make_field();           // заполняем поля

    Status status;              // поле, для хранени статуса
    SettingGame setting;


};

#endif // GAME_H
