#ifndef REACTION_H
#define REACTION_H

#include "direction.h"
#include "Object/type_object.h"

// используется для возвращения реакции объекта обратно в игру

struct Reaction
{
public:

    bool is_death;          // сообщает о своей смерти
    bool need_move;         // объект сообщает о необходимости передвижения
    DIRECTION direction;    // и желаемом направлении


    TYPE_OBJECT new_object; // и о рождении нового объекта
};


#endif // REACTION_H

