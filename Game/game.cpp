#include "game.h"


Game::Game(SettingGame _setting):size_field(_setting.size_field),setting(_setting)
{


    status.generation = 0;
    status.count_all_object = 0;
    status.count_predator = 0;
    status.count_prey = 0;

    initial();
    std::srand(time(0));



}

Game::~Game()
{

    for(int i = 0; i < size_field.x; i++)
    {
        delete[] (next_field[i]);
        delete[] (current_field[i]);
    }

    delete[](next_field);
    delete[](current_field);
}
void Game::swap_field()
{
    TYPE_OBJECT ** temp = current_field;

    current_field = next_field;
    next_field = temp;
}


void Game::initial()
{
    next_field = new TYPE_OBJECT * [size_field.x];
    current_field = new TYPE_OBJECT * [size_field.x];
    array_field = new Object ** [size_field.x];

    for(int i = 0; i < size_field.x; i++)
    {
        next_field[i] = new TYPE_OBJECT [size_field.y];
        current_field[i] = new TYPE_OBJECT [size_field.y];
        array_field[i] = new Object * [size_field.y];

        for(int j = 0; j < size_field.y; j++)
        {
            next_field[i][j] = TYPE_OBJECT::GRAY;
            current_field[i][j] = TYPE_OBJECT::GRAY;
            array_field[i][j] = nullptr;
        }
    }


    array_object.push_back( new Prey( Coord(0,1),setting.setting_prey));
    array_object.push_back( new Prey( Coord(0,2),setting.setting_prey));
    array_object.push_back( new Prey( Coord(0,3),setting.setting_prey));
    array_object.push_back( new Prey( Coord(0,4),setting.setting_prey));
    array_object.push_back( new Prey( Coord(0,5),setting.setting_prey));
    array_object.push_back( new Prey( Coord(0,6),setting.setting_prey));
    array_object.push_back( new Prey( Coord(1,1),setting.setting_prey));
    array_object.push_back( new Prey( Coord(1,2),setting.setting_prey));
    array_object.push_back( new Prey( Coord(1,3),setting.setting_prey));
    array_object.push_back( new Prey( Coord(1,4),setting.setting_prey));
    array_object.push_back( new Prey( Coord(1,5),setting.setting_prey));
    array_object.push_back( new Prey( Coord(1,6),setting.setting_prey));

    array_object.push_back( new Prey( Coord(3,1),setting.setting_prey));
    array_object.push_back( new Prey( Coord(3,2),setting.setting_prey));
    array_object.push_back( new Prey( Coord(3,3),setting.setting_prey));
    array_object.push_back( new Prey( Coord(3,4),setting.setting_prey));
    array_object.push_back( new Prey( Coord(3,5),setting.setting_prey));
    array_object.push_back( new Prey( Coord(3,6),setting.setting_prey));
    array_object.push_back( new Prey( Coord(4,1),setting.setting_prey));
    //array_object.push_back( new Prey( Coord(4,2)));
    array_object.push_back( new Prey( Coord(5,3),setting.setting_prey));
    array_object.push_back( new Prey( Coord(4,4),setting.setting_prey));
    array_object.push_back( new Prey( Coord(4,5),setting.setting_prey));
    array_object.push_back( new Prey( Coord(5,6),setting.setting_prey));

    array_object.push_back( new Predator( Coord(1,0),setting.setting_predator));
    array_object.push_back( new Predator( Coord(4,2),setting.setting_predator));
    make_field();


}

TYPE_OBJECT Game::getObject(Coord cell)
{
    return current_field[cell.x][cell.y];
}


void Game::make_field()
{
    Coord crd;
    Status temp_status;


    temp_status.generation = status.generation + 1;
    temp_status.count_all_object = 0;
    temp_status.count_predator = 0;
    temp_status.count_prey = 0;

    for(int i = 0; i < size_field.x; i++)
    {

        for(int j = 0; j < size_field.y; j++)
        {
            next_field[i][j] = TYPE_OBJECT::GRAY;
            array_field[i][j] = nullptr;
        }
    }


    for(auto i:array_object)
    {
        crd = i->get_position();
        array_field[crd.x][crd.y] = i;
        next_field[crd.x][crd.y] = i->get_type();

        temp_status.count_all_object++;
        switch (next_field[crd.x][crd.y])
        {
        case TYPE_OBJECT::PREDATOR:
            temp_status.count_predator++;
            break;
        case TYPE_OBJECT::PREY:
            temp_status.count_prey++;
            break;
        default:
            break;
        }
    }

    status = temp_status;

}

void Game::nextStep()
{


    Environment env;
    Reaction reac;
    Coord _coord;
    std::vector<Object *> temp_array_object(array_object);


    array_object.clear();

    for(auto i:temp_array_object)
    {


        env.free_down = false;
        env.free_up = false;
        env.free_left = false;
        env.free_right = false;

        env.neighbors_down = TYPE_OBJECT::NONE;
        env.neighbors_up = TYPE_OBJECT::NONE;
        env.neighbors_left = TYPE_OBJECT::NONE;
        env.neighbors_right = TYPE_OBJECT::NONE;

        env.set_death = false;


        _coord = i -> get_position();

        if(_coord.y < size_field.y-1)
        {
            env.free_down = ( nullptr == array_field[_coord.x][_coord.y+1])?true:false;
            if( !env.free_down )
                env.neighbors_down = (array_field[_coord.x][_coord.y+1])->get_type();
        }

        if(_coord.x < size_field.x-1)
        {
            env.free_right = ( nullptr == array_field[_coord.x+1][_coord.y])?true:false;
            if( !env.free_right )
                env.neighbors_right = (array_field[_coord.x+1][_coord.y])->get_type();
        }

        if(_coord.y > 0)
        {
            env.free_up = ( nullptr == array_field[_coord.x][_coord.y-1])?true:false;
            if( !env.free_up )
                env.neighbors_up = (array_field[_coord.x][_coord.y-1])->get_type();
        }

        if(_coord.x > 0)
        {
            env.free_left = ( nullptr == array_field[_coord.x-1][_coord.y])?true:false;
            if( !env.free_left )
                env.neighbors_left = (array_field[_coord.x-1][_coord.y])->get_type();
        }

        if( 0 == _coord.x )
        {
            env.free_left = ( nullptr == array_field[size_field.x - 1][_coord.y])?true:false;
            if( !env.free_left )
                env.neighbors_left = (array_field[size_field.x - 1][_coord.y])->get_type();
        }

        if( size_field.x - 1 == _coord.x )
        {
            env.free_right = ( nullptr == array_field[0][_coord.y])?true:false;
            if( !env.free_right )
                env.neighbors_right = (array_field[0][_coord.y])->get_type();
        }

        if( 0 == _coord.y )
        {
            env.free_up = ( nullptr == array_field[_coord.x][size_field.y - 1])?true:false;
            if( !env.free_up )
                env.neighbors_up = (array_field[_coord.x][size_field.y - 1])->get_type();
        }

        if( size_field.y - 1 == _coord.y )
        {
            env.free_down = ( nullptr == array_field[_coord.x][0])?true:false;
            if( !env.free_down )
                env.neighbors_down = (array_field[_coord.x][0])->get_type();
        }


        reac =  i->action(env);

        if(!reac.is_death)
        {


            Object * tmp = nullptr;

            if( ( TYPE_OBJECT::NONE != reac.new_object ) && reac.need_move)
            {
                switch(i->get_type())
                {
                case TYPE_OBJECT::PREDATOR:
                    tmp = new Predator(_coord,setting.setting_predator);
                    array_object.push_back( tmp);
                    break;
                case TYPE_OBJECT::PREY:
                    tmp = new Prey(_coord, setting.setting_prey);
                    array_object.push_back( tmp);
                    break;
                default:
                    break;
                }
             }

            array_field[_coord.x][_coord.y] = tmp;

            if(reac.need_move)
            {

                switch ( reac.direction)
                {
                case DIRECTION::DOWN:
                    _coord.y++;
                    if( _coord.y > size_field.y -1 )
                        _coord.y = 0;
                    break;
                case DIRECTION::RIGHT:
                    _coord.x++;
                    if( _coord.x > size_field.x - 1)
                        _coord.x = 0;
                    break;
                case DIRECTION::LEFT:
                    _coord.x--;
                    if( _coord.x < 0)
                        _coord.x = size_field.x - 1;
                    break;
                case DIRECTION::UP:
                    _coord.y--;
                    if( _coord.y < 0)
                        _coord.y = size_field.y - 1;
                    break;
                default:
                    break;
                }
            }

            if( TYPE_OBJECT::PREDATOR == i -> get_type())
            {
                if( nullptr != array_field[_coord.x][_coord.y])
                {
                    (array_field[_coord.x][_coord.y])->set_death();
                   (static_cast<Predator *> (i))-> set_hangry();
                }
            }

            i -> set_position(_coord);
            array_field[_coord.x][_coord.y] = i;
            array_object.push_back(i);


        }


    }

    make_field();
    swap_field();

}

Status Game::getStatus()
{
    return status;
}
