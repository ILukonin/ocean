#ifndef DIRECTION_H
#define DIRECTION_H

enum class DIRECTION {UP, DOWN, LEFT, RIGHT};

#endif // DIRECTION_H

