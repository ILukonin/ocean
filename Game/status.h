#ifndef STATUS_H
#define STATUS_H

struct Status
{
public:
    unsigned long count_all_object;
    unsigned long count_predator;
    unsigned long count_prey;
    unsigned long generation;

};


#endif // STATUS_H

