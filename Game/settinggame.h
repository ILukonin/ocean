#ifndef SETTINGGAME_H
#define SETTINGGAME_H

#include "coord.h"
#include "Object/setting.h"

struct SettingGame
{
public:

    Coord size_field;
    unsigned int time_interval;

    SettingObjectPredator setting_predator;
    SettingObjectPrey setting_prey;
};

#endif // SETTINGGAME_H

