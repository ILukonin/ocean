#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include "Object/type_object.h"
//используется для передачи информации объектам об окружающих их ячейках

struct Environment
{
public:

    TYPE_OBJECT neighbors_up;   // тип соседей
    TYPE_OBJECT neighbors_down;
    TYPE_OBJECT neighbors_left;
    TYPE_OBJECT neighbors_right;

    bool free_up;               // свободна ли клетка от препятствий
    bool free_down;
    bool free_left;
    bool free_right;

    bool set_death;             // передаём сигнал объекту умереть

};

#endif // ENVIRONMENT_H

