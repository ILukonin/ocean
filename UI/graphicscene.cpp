#include "graphicscene.h"

#include <QGraphicsItem>
#include <QtConcurrent/QtConcurrent>

#include "Object/type_object.h"
#include "coord.h"




GraphicScene::GraphicScene(int size_x, int size_y, QObject * parent ):
    QGraphicsScene(parent),size_field(size_x, size_y),update_flag(false)
{
    SettingGame setting;


    setting.size_field = size_field;

    setting.setting_predator.birth_count = 30;
    setting.setting_predator.hangry      = 5;
    setting.setting_predator.hangry_after_birth = 5;
    setting.setting_predator.lifetime = 25;
    setting.setting_predator.hangry_prey = 5;

    setting.time_interval = 1;

    setting.setting_prey.birth_count = 3;
    setting.setting_prey.lifetime = 50;

    game =  new Game(setting);

    size_cell.x = 20;
    size_cell.y = 20; // размер одного тайла


    make_background();

    timer = new QTimer(this);

    connect(timer,&QTimer::timeout, this, &GraphicScene::update_fill_timer);
    timer->start(setting.time_interval);

}

GraphicScene::~GraphicScene()
{

}

void GraphicScene::make_background()
{

    for(int i = 0; i < size_field.x;i++)
    {
        for(int j = 0; j < size_field.y; j++)
        {
            this->addItem(new Cell(game,Coord(i,j),size_cell));
        }
    }

}


void GraphicScene::update_fill()
{

    game->nextStep(); // делаем шаг в игре
    this->advance(); // шлём сообщение всем итемам, чтоб обновились
    emit updated();
    this->update();  // перерисовываемся
    update_flag = false;

}

Status GraphicScene::get_status()
{
    return this->game->getStatus();
}

void GraphicScene::update_fill_timer()
{
    if(!update_flag)
    {
        update_flag = true;
        QtConcurrent::run(this, &GraphicScene::update_fill); // запустили обновление в отдельном треде
    }
}


