#include "graphicsview.h"
#include <QWheelEvent>

GraphicsView::GraphicsView( QGraphicsScene * scene):QGraphicsView(scene)
{

}

GraphicsView::~GraphicsView()
{

}

void GraphicsView::zoom_in()
{
    scale(1.2,1.2);
}

void GraphicsView::zoom_out()
{
    scale(1/1.2,1/1.2);
}

void GraphicsView::wheelEvent(QWheelEvent * event)
{
    if(event->delta() > 0)
        zoom_in();
    else
        zoom_out();
}
