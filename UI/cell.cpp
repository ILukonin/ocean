#include "cell.h"

#include <QPen>
#include <QPainter>


Cell::Cell(Game *_game, Coord _position, Coord _cell_size):
    game(_game),position(_position),cell_size(_cell_size)
{

    //создаем тайлы
    tile_grey     = new QPixmap("images/gray.png");
    tile_tree     = new QPixmap("images/tree.png");
    tile_stoun    = new QPixmap("images/stoun.png");
    tile_prey     = new QPixmap("images/pray.png");
    tile_predator = new QPixmap("images/predator.png");

    cell_tipe = game->getObject(position); // узнаём тип ячейки
    this->setPos(position.x * cell_size.x, position.y * cell_size.y);

}

Cell::~Cell()
{

}

QRectF Cell::boundingRect() const
{
    return QRectF(0,0,cell_size.x,cell_size.y);
}

void Cell::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    painter->drawPixmap(0,0,cell_size.x, cell_size.y,*tile_grey);

    switch (cell_tipe)
    {
    case TYPE_OBJECT::PREY:
        painter->drawPixmap(0,0,cell_size.x,cell_size.y,*tile_prey);
        break;
    case TYPE_OBJECT::PREDATOR:
        painter->drawPixmap(0,0,cell_size.x,cell_size.y,*tile_predator);
        break;
    case TYPE_OBJECT::TREE:
        painter->drawPixmap(0,0,cell_size.x,cell_size.y,*tile_tree);
        break;
    case TYPE_OBJECT::STOUN:
        painter->drawPixmap(0,0,cell_size.x,cell_size.y,*tile_stoun);
        break;
    default:
        break;
    }

}

void Cell::advance(int phase)
{

    if(1 == phase) // обновляемся только тогда. когда сцена готова к перерисовке
        cell_tipe = game->getObject(position);

}
