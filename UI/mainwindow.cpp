#include <QString>

#include "mainwindow.h"
#include "Game/status.h"


MainWindow::MainWindow(QWidget *parent): QWidget(parent)
{
    Game = new GraphicScene(100,100);
    View = new GraphicsView(Game);

    label_all_object = new QLabel("ALL");
    label_prey       = new QLabel("Prey");
    label_predator   = new QLabel("Predator");
    label_generation = new QLabel("Generation");

    label_all_object->setMaximumWidth(200);
    label_all_object->setMinimumWidth(200);


    layout_main  = new QHBoxLayout(this);
    layout_label = new QVBoxLayout(this);


    layout_main->addWidget(View);
    layout_main->addStretch(4);
    layout_main->addLayout(layout_label);

    layout_label->addWidget(label_all_object);
    layout_label->addWidget(label_predator);
    layout_label->addWidget(label_prey);
    layout_label->addWidget(label_generation);
    layout_label->addStretch(4);

    connect(Game,&GraphicScene::updated,this,&MainWindow::updated_game);

}

MainWindow::~MainWindow()
{

}

void MainWindow::updated_game()
{
    Status status = Game->get_status();
    label_all_object->setText( "Всего: " + QString::number(status.count_all_object) );
    label_prey->setText( "Жертв: " + QString::number(status.count_prey) );
    label_predator->setText( "Хищников: " + QString::number(status.count_predator) );
    label_generation->setText( "Поколение: " + QString::number(status.generation) );
}
