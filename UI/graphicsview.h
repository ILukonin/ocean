#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>

class GraphicsView: public QGraphicsView
{
    Q_OBJECT

public:
    GraphicsView(  QGraphicsScene * scene);
    ~GraphicsView();



public slots:
    void zoom_in();
    void zoom_out();

protected:
    void wheelEvent(QWheelEvent * event);


};

#endif // GRAPHICSVIEW_H
