#ifndef CELL_H
#define CELL_H

#include <QGraphicsItem>

#include "Object/object.h"
#include "Game/game.h"
#include "coord.h"

class Cell: public QGraphicsItem
{
public:
    Cell(Game * _game, Coord _position, Coord _cell_size);
    ~Cell();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

public slots:
    void advance(int phase);

protected:
    Coord position;
    Coord cell_size;
    TYPE_OBJECT cell_tipe; // тип ячейки
    QPixmap * tile_prey; // тут будем хранить тайлы. будем показывать
    QPixmap * tile_predator; // по мере необходимости, остальное скроем.
    QPixmap * tile_tree;
    QPixmap * tile_stoun;
    QPixmap * tile_grey;

    Game * game; // указатель на игру

};

#endif // CELL_H
