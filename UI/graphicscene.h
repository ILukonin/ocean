#ifndef GRAPHICSCENE_H
#define GRAPHICSCENE_H

#include <QGraphicsScene>
#include <QTimer>

#include "coord.h"
#include "Game/game.h"
#include "cell.h"
#include "Game/status.h"
#include "Game/settinggame.h"


class GraphicScene: public QGraphicsScene
{
    Q_OBJECT

public:
    GraphicScene(int size_x = 10, int size_y = 10, QObject * parent = 0);
    ~GraphicScene();

    Status get_status();

public slots:

    void update_fill_timer(); // слот для таймера

signals:
    void updated();

protected:


    Coord size_field;
    Coord size_cell;
    Game * game;
    QTimer * timer; // игровой таймер

    bool update_flag; // флаг обработки

    void make_background();
    void update_fill();

};

#endif // GRAPHICSCENE_H
