#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "graphicscene.h"
#include "graphicsview.h"



class MainWindow: public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void updated_game();

protected:
    GraphicScene * Game;
    GraphicsView * View;

    QLabel * label_all_object;
    QLabel * label_prey;
    QLabel * label_predator;
    QLabel * label_generation;

    QVBoxLayout * layout_label;
    QHBoxLayout * layout_main;


};

#endif // MAINWINDOW_H
